var express = require('express');
var app = express();

const PORT = process.env.PORT || "8080";
app.set("port", PORT);
    
/*
 * Server
 */
const { createServer } = require('http');
const server = createServer(app);

/*  
 * Init Cache DB. Redis Singleton.
 */
import { redisManager } from './framework/db/redis.manager';

/*
 * Init Mongo DB
 */
import MongooseManager from './framework/db/mongoose.manager';
const db = new MongooseManager();
// Connect and Load models
import BearerSchema from './models/Bearer/bearer.mongo.model';
import AuthSchema from './models/Auth/auth.mongo.model'
db.connectMongo().then(() => {
    new BearerSchema(db.getMongoose())
    new AuthSchema(db.getMongoose())
    const mongoose = db.getMongoose()
    console.log("\nMongo models loaded\n======================")
    console.log(mongoose.models)
})

/*
 * Config common Middlewares: bodyparser, cors, morgan
 */
import { ConfigCommonMiddleware } from './middlewares/commonMiddleware.config'
const configCommonMiddlewares = new ConfigCommonMiddleware(app) 
configCommonMiddlewares.applyMiddlewares()

/*
 * Error handler middleware
 */
import { ErrorHandler } from './middlewares/error.middleware'
const errorHandler = new ErrorHandler(app);
errorHandler.applyMiddlewares()

/*
 * Auth middleware. Define only if you are going to use it in some Directors or in graphQL
 */
import { JWTManager } from './framework/utils/jwt.manager';
import { AuthMiddleware } from './middlewares/jwt.middleware';
if (!process.env.JWT_TOKEN) {
    throw new Error("NOT TOKEN DEFINED")
}
const jwtManager = new JWTManager(process.env.JWT_TOKEN, process.env.JWT_TOKEN_EXPIRATION)
const authMiddleware = new AuthMiddleware(jwtManager)

/*
 * Caché middleware. Need redis
 */
import { CacheMiddleware } from './framework/utils/cache.middleware';
const cacheMiddleware = new CacheMiddleware(app, redisManager)
app.use(cacheMiddleware.cacheSave)

/*
 * Instance Directors and routes
 */
import { Routes } from './Route'
import { BaseRoute } from './framework/base/route.interface';
const routes: Array<BaseRoute> = []

// Bearer Resource
console.log("\nRESOURCES\n======================")
import { AuthDBDirector } from './models/Auth/auth.director';
const authDirector = new AuthDBDirector(db, undefined, authMiddleware, cacheMiddleware, jwtManager)
authDirector.buildMongo()
authDirector.getRoutes().forEach(route => routes.push(route))

import { BearerDBDirector } from './models/Bearer/bearer.director';
const bearerDirector = new BearerDBDirector(db, undefined, authMiddleware, cacheMiddleware, jwtManager)
bearerDirector.buildMongo()
bearerDirector.getRoutes().forEach(route => routes.push(route))

// Auth Resource
// import { AuthDBDirector } from './models/Auth/auth.director';
// let authDirector: AuthDBDirector | undefined = undefined
// if (redisManager) {
//     authDirector = new AuthDBDirector(redisManager, undefined, authMiddleware, cacheMiddleware, jwtManager)
//     authDirector.buildRedis()
//     authDirector.getRoutes().forEach(route => routes.push(route))
// } else {
//     throw new Error("Trying to user Redis Manager from Auth Routes but not declared or undefined") 
// }

// Integration with Microservices

//  DESDE FUERA: HTTP 80 y 443 - i360-i04-cgm.insulclock360-2-4x16-pro-eaf5bc950b7aa3accb3329ec6e66e231-0000.eu-de.containers.appdomain.cloud => no hace falta apuntar al puerto
//  DENTRO DEL MISMO CLUSTER Y MISMO NAMESPACE http://i04-cgm-host:8080/api/v1 => si hace falta el puerto
//  DESDE OTRO NAMESPACE: http://i04-cgm-host.i360.svc.cluster.local:8080/api/v1 => i360 -> namespace original
// import { CrypRoutes } from './microservices/cryp/cryp.routes'
// let crypRoutes = new CrypRoutes("http://i04-cgm-host:8080/api/v1", authMiddleware, cacheMiddleware)
// crypRoutes.getRoutes().forEach(route => routes.push(route))

import { I02CommunicationRoutes } from './microservices/i02-communications/i02-communications.routes'
let i02CommunicationRoutes = new I02CommunicationRoutes(authMiddleware, undefined)
i02CommunicationRoutes.getRoutes().forEach(route => routes.push(route))

/*
 * Define app namespace and version
 */
const prefix = "/api/v1"

/*
 * Create routes
 */
new Routes(app, prefix, routes);

/*
 * GraphQL
 */
/// import { GraphqlObjectLog } from './graphql/log.schema';
// import GraphQLManager from './framework/db/graphql.manager';

// let graphQLObjects = []
// const graphqlObjectLog = new GraphqlObjectLog(logDirector.builder.getController()!)
// graphQLObjects.push(graphqlObjectLog)

// import { GraphqlObjectCryp } from './graphql/cryp.schema';
// let graphQLMicroservicesObjects = []
// const graphqlObjectMicroserviceCryp = new GraphqlObjectCryp("http://172.16.235.7:8081/api/v1")
// graphQLMicroservicesObjects.push(graphqlObjectMicroserviceCryp)

// const redisPublisher = redisManager.getRedisClientPublisher()
// const redisSubscriber = redisManager.getRedisClientSubscriber()
// let graphQLManager = new GraphQLManager(app, [], graphQLMicroservicesObjects, authMiddleware, redisPublisher, redisSubscriber);
// graphQLManager.generateGraphQLSchema()
// graphQLManager.configGraphQLRouteAndServe(prefix, PORT)

/*
 * Resource not found. Important define after routes
 */
import { notFoundHandler } from "./middlewares/not-found.middleware";
app.use(notFoundHandler);

/*
 * Start server
 */
server.listen(PORT, () => {
    console.info(`\nServer starting and running on host:${PORT}${prefix}`);

    // Use only if graphQL is active and subscriptions are enabled (only by graphql-ws protocols)
    // graphQLManager.getWSServer(server, prefix) 
    // console.info(`Server running api and GraphQL on ${prefix}/graphql`);
    
    console.info(`======================`)
});

// app.listen(PORT, () => console.log(`Server running on localhost:${PORT}`));
