import { Route } from '../../models/route.model'
import { BaseRoute } from '../../framework/base/route.interface';
import { AuthMiddlewareInterface } from '../../framework/utils/jwt.middleware.interface';
import { httpRequest } from '../../framework/utils/fetch.manager';
import { CacheMiddleware } from '../../framework/utils/cache.middleware';

export class CrypRoutes {
    routes: BaseRoute[] = []
    urlBase: string
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware
    
    constructor(urlBase: string, authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        this.urlBase = urlBase
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const middlewares = []
        if (this.authenticationMiddleware) {
           middlewares.push(this.authenticationMiddleware.authenticate)
        }
        const middlewaresWithCache = []
        if (this.cacheMiddleware) {
            middlewaresWithCache.push(this.cacheMiddleware.cacheCheck)
        }

        let urlBase = this.urlBase
        this.routes = [
            new Route('get', '/cryp/:id', middlewares.concat(middlewaresWithCache), async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/cryp/${req.params.id}`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            // new Route('post', '/cryp', middlewares, this.handler.create),
            // new Route('delete', '/cryp/:id', middlewares, this.handler.delete),
        ];
    }

    getRoutes() {
        return this.routes
    }
}