import { Route } from '../../models/route.model'
import { BaseRoute } from '../../framework/base/route.interface';
import { AuthMiddlewareInterface } from '../../framework/utils/jwt.middleware.interface';
import { httpRequest } from '../../framework/utils/fetch.manager';
import { CacheMiddleware } from '../../framework/utils/cache.middleware';

export class I02CommunicationRoutes {
    routes: BaseRoute[] = []
    urlBase: string
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware
    
    constructor(authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        if (!process.env.I02_COMMUNICATIONS_URL) {
            throw new Error("NO COMMUNICATION URL PROVIDED")
        }
        this.urlBase = process.env.I02_COMMUNICATIONS_URL // For DEV, ingress should be active
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const middlewares = []
        if (this.authenticationMiddleware) {
           middlewares.push(this.authenticationMiddleware.authenticate)
        }
        const middlewaresWithCache = []
        if (this.cacheMiddleware) {
            middlewaresWithCache.push(this.cacheMiddleware.cacheCheck)
        }

        let urlBase = this.urlBase
        this.routes = [
            new Route('get', '/msg/', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/msg/`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('get', '/msg/:id', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/msg/${req.params.id}`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('post', '/msg', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/msg`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('put', '/msg/:id', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/msg/${req.params.id}`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('delete', '/msg/:id', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/msg/${req.params.id}`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('post', '/sms', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/sms`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('post', '/smsStatusChange', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/smsStatusChange`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('post', '/generateTokenTwilio', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/generateTokenTwilio`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            new Route('post', '/connectCall', middlewares, async function(req, res, next) {
                // res.redirect(301, `${urlBase}/cryp/${req.params.id}`)    
                const url = `${urlBase}/connect`
                try {
                    const response = await httpRequest(url, req)
                    return res.send(response)
                } catch (error) {
                    return res.status(400).send(error)
                }
            }),
            // new Route('post', '/cryp', middlewares, this.handler.create),
            // new Route('delete', '/cryp/:id', middlewares, this.handler.delete),
        ];
    }

    getRoutes() {
        return this.routes
    }
}