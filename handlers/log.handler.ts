import { Handler } from '../framework/base/handler.model';
import { ControllerInterface } from '../framework/base/controller.interface';
import { Pagination } from '../framework/utils/pagination.interface';
import { LogSearchFields } from '../models/Logs/log.searchFields';
import { LogInterface } from '../models/Logs/log.model';
import { HandlerInterface } from '../framework/base/handler.interface';
import { Console } from 'node:console';

export class LogHandler implements HandlerInterface<LogInterface> {
    controller: ControllerInterface<LogInterface>;

    constructor(controller: ControllerInterface<LogInterface>) {
        this.controller = controller
    }

    list: Handler = async (req, res) => {
        try {
            // console.log(req.body.credentials)
            const { page = 1, limit = 100, dateStart = "", dateEnd = "", serviceProvider = "" } = req.query;
            const pagination = new Pagination(Number(page), Number(limit), true)
            const searchFields = new LogSearchFields( { startDate: String(dateStart), endDate: String(dateEnd)}, String(serviceProvider))
            let key = ""
            if (this.controller.cacheDataBase) { 
                key = this.controller.cacheDataBase.getListCacheKey("logs", [searchFields.byDate.startDate, searchFields.byDate.endDate, searchFields.byServiceProvider, String(pagination.limit), String(pagination.page)])
                const data = await this.controller.cacheDataBase.getArray<LogInterface>(key)
                if (data && data.items && data.pagination) {
                    const items = data.items
                    const pagination = data.pagination
                    console.log("*** Data from cache")
                    return res.status(201).send({items: items, pagination: pagination, cache: "true"}); 
                }
            }

            const logs = await this.controller.list(searchFields, pagination)
            if (this.controller.cacheDataBase) {
                await this.controller.cacheDataBase.saveArray(key, logs.items, logs.pagination)
            }
            return res.status(201).send(logs);
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }
    show: Handler = async (req, res) => {
        const id = req.params.id
        if (!id?.trim()) {
            return res.status(400).send('Bad request');
        }

        try {
            let key = ""
            if (this.controller.cacheDataBase) { 
                key = this.controller.cacheDataBase.getObjectCacheKey("logs", id)
                const logToShow = await this.controller.cacheDataBase.getObject(key)
                if (logToShow) {
                    const item = logToShow as LogInterface
                    console.log("*** Data from cache")
                    return res.status(201).send({item: item, cache: "true"});
                }
            }

            const log = await this.controller.show(id)
            if (!log) {
                return res.status(400).send("Resource not found")
            }
            if (this.controller.cacheDataBase) {
                this.controller.cacheDataBase.saveObject("logs", log)
            }
            res.status(201).send(log);
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }
    create: Handler = async (req, res) => {
        try {
            const { log, serviceProvider } = req.body
            if (!log || !serviceProvider) {
                return res.status(400).send('Bad request');
            }
            const logCreated = await this.controller.create(req.body)
            if (!logCreated) {
                res.status(400).send("Something was wrong")
            }
            if (this.controller.cacheDataBase) {
                await this.controller.cacheDataBase.clearCache("logs:list:*")
                await this.controller.cacheDataBase.saveObject("logs", logCreated)
            }
            return res.send(logCreated) 
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }
    update: Handler = async (req, res) => {
        try {
            const id = req.params.id
            if (!id?.trim()) {
                return res.status(400).send('Bad request');
            }

            const { log, serviceProvider } = req.body
            if (!log) {
                return res.status(400).send('Bad request');
            }
            let params : any = {
                log: log
            }
            if (serviceProvider) {
                params.serviceProvider = serviceProvider
            }

            const logUpdated = await this.controller.update(id, params)
            if (!logUpdated) {
                return res.status(400).send("Resource not found")
            }
            if (this.controller.cacheDataBase) {
                await this.controller.cacheDataBase.clearCache("logs:list:*")
                await this.controller.cacheDataBase.saveObject("logs", logUpdated)
            }
            return res.send(logUpdated) 
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }
    delete: Handler = async (req, res) => {
        const id = req.params.id
        if (!id?.trim()) {
            return res.status(400).send('Bad request');
        }

        try {
            const log = await this.controller.delete(id)
            if (!log) {
                res.status(400).send("Resource not found")
            }
            if (this.controller.cacheDataBase) {
                await this.controller.cacheDataBase.clearCache("logs:list:*")
                await this.controller.cacheDataBase.delAsync("logs:"+ log!._id)   
            }
            return res.status(200).send(log)
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }
    
}