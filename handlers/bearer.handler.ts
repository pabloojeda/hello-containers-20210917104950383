import { Handler } from '../framework/base/handler.model';
import { ControllerInterface } from '../framework/base/controller.interface';
import { HandlerReadInterface } from '../framework/base/handler.interface';
import { JWTManager } from '../framework/utils/jwt.manager';
import { ErrorType, getErrorJSON } from '../framework/utils/errors';
import { BearerInterface } from '../models/Bearer/bearer.model';
import { AuthController } from '../controllers/auth.controller';
import { AuthManagerMongo } from '../managers/Auth/auth.manager.mongo';

export class BearerHandler implements HandlerReadInterface<BearerInterface> {
    controller: ControllerInterface<BearerInterface>;
    jwtManager: JWTManager

    constructor(controller: ControllerInterface<BearerInterface>, jwtManager: JWTManager) {
        this.controller = controller
        this.jwtManager = jwtManager
    }

    list: Handler = async (req, res) => {
        try {
            const bearer = await this.controller.list(undefined, undefined)
            return res.status(201).send(bearer);
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    show: Handler = async (req, res) => {
        const token = req.params.token
        if (!token?.trim()) {
            return res.status(400).send(getErrorJSON(ErrorType.BadRequestParams));
        }

        try {
            const bearer = await this.controller.show(token)
            if (!bearer) {
                return res.status(400).send(getErrorJSON(ErrorType.ResourceNotFound))
            }
            res.status(201).send(bearer);
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    getBearerToken: Handler = async (req, res) => {
        try { 
            const { user, apiKey } = req.body
            if (!user || !apiKey) {
                return res.status(401).send(getErrorJSON(ErrorType.BadRequestParams))
            }

            const authManager = new AuthManagerMongo(this.controller.dataBase.database)
            const auth = await authManager.show(user)
            
            if (!auth) {
                return res.status(401).send(getErrorJSON(ErrorType.Unauthorized))
            }
            if (!auth.apiKey || (auth.apiKey && auth.apiKey !== apiKey) ) {
                return res.status(401).send(getErrorJSON(ErrorType.Unauthorized))
            }
            if (!process.env.JWT_TOKEN) {
                res.status(500).send(getErrorJSON(ErrorType.Generic))
                throw new Error("NOT TOKEN DEFINED")
            }

            //const jwtManager = new JWTManager(process.env.JWT_TOKEN, this.ttl)
            const token = await this.jwtManager.generateAccessToken(auth)
            const tokenDecoded = await this.jwtManager.verifyToken(token)
            tokenDecoded.token = token
            if (tokenDecoded["_id"]) {
                tokenDecoded["authID"] = tokenDecoded["_id"]
                delete tokenDecoded["_id"]
            }
            await this.controller.create(tokenDecoded)

            return res.send({token: token, expired: tokenDecoded.exp ? tokenDecoded.exp : "" })
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
}