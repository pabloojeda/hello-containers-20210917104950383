import { Handler } from '../framework/base/handler.model';
import { ControllerInterface } from '../framework/base/controller.interface';
import { HandlerAuthInterface } from '../framework/base/handler.interface';
import { AuthInterface } from '../models/Auth/auth.model';
import { JWTManager } from '../framework/utils/jwt.manager';
import { ErrorType, getErrorJSON } from '../framework/utils/errors';

export class AuthHandler implements HandlerAuthInterface<AuthInterface> {
    controller: ControllerInterface<AuthInterface>;
    jwtManager: JWTManager

    constructor(controller: ControllerInterface<AuthInterface>, jwtManager: JWTManager) {
        this.controller = controller
        this.jwtManager = jwtManager
    }

    list: Handler = async (req, res) => {
        try {
            const auths = await this.controller.list(undefined, undefined)
            return res.status(201).send(auths);
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    show: Handler = async (req, res) => {
        const user = req.params.id
        if (!user?.trim()) {
            return res.status(400).send(getErrorJSON(ErrorType.BadRequestParams));
        }

        try {
            const auth = await this.controller.show(user)
            if (!auth) {
                return res.status(400).send(getErrorJSON(ErrorType.ResourceNotFound))
            }
            res.status(201).send(auth);
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    create: Handler = async (req, res) => {
        try {
            const { user } = req.body
            if (!user) {
                return res.status(400).send(getErrorJSON(ErrorType.BadRequestParams))
            } else {
                req.body.userID = user
            }
            
            const auth = await this.controller.create(req.body)
            return res.send(auth) 
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    update: Handler = async (req, res) => {
        try {
            const id = req.params.id
            if (!id?.trim()) {
                return res.status(400).send(getErrorJSON(ErrorType.BadRequestParams))
            }
            const auth = await this.controller.update(id, req.body)
            if (!auth) {
                res.status(400).send(getErrorJSON(ErrorType.ResourceNotFound))
            }
            return res.send(auth) 
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
    delete: Handler = async (req, res) => {
        const id = req.params.id
        if (!id?.trim()) {
            return res.status(400).send(getErrorJSON(ErrorType.BadRequestParams))
        }
        try {
            const auth = await this.controller.delete(id)
            if (!auth) {
                res.status(400).send(getErrorJSON(ErrorType.ResourceNotFound))
            }
            return res.status(200).send(auth)
        } catch (error) {
            console.log(error)
            return res.status(400).send(getErrorJSON(ErrorType.Generic))
        }
    }
}