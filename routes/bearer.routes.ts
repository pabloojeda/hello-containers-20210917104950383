// import { Route } from '../types';
import { Route } from '../models/route.model'
import { BaseRoute, RoutesReadInterface } from '../framework/base/route.interface';
import { BearerInterface } from '../models/Bearer/bearer.model';
import { AuthMiddlewareInterface } from '../framework/utils/jwt.middleware.interface';
import { CacheMiddleware } from '../framework/utils/cache.middleware';
import { BearerHandler } from '../handlers/bearer.handler';

export class BearerRoutes implements RoutesReadInterface<BearerInterface> {
    handler: BearerHandler
    routes: BaseRoute[] = []
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware

    constructor(handler: BearerHandler, authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        this.handler = handler
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const middlewares = []
        if (this.authenticationMiddleware) {
            middlewares.push(this.authenticationMiddleware.authenticate, this.authenticationMiddleware.hasRole("admin"))
        }

        const middlewaresWithCache = []
        if (this.cacheMiddleware) {
            middlewaresWithCache.push(this.cacheMiddleware.cacheCheck)
        }
        
        this.routes = [
            new Route('get', '/bearer/', middlewares,/* middlewares.concat(middlewaresWithCache) */ this.handler.list),
            new Route('get', '/bearer/:token', middlewares, this.handler.show),
            // new Route('post', '/bearer', middlewares, this.handler.create),
            // new Route('put', '/bearer/:id', middlewares, this.handler.update),
            // new Route('delete', '/bearer/:id', middlewares, this.handler.delete),
            new Route('post', '/bearer/getBearerToken', [], this.handler.getBearerToken),
        ];
    }
}