// import { Route } from '../types';
import { Route } from '../models/route.model'
import { BaseRoute, RoutesInterface } from '../framework/base/route.interface';
import { AuthInterface } from '../models/Auth/auth.model';
import { AuthHandler } from '../handlers/auth.handler';
import { AuthMiddlewareInterface } from '../framework/utils/jwt.middleware.interface';
import { CacheMiddleware } from '../framework/utils/cache.middleware';

export class AuthRoutes implements RoutesInterface<AuthInterface> {
    handler: AuthHandler
    routes: BaseRoute[] = []
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware

    constructor(handler: AuthHandler, authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        this.handler = handler
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const middlewares = []
        if (this.authenticationMiddleware) {
            middlewares.push(this.authenticationMiddleware.authenticate, this.authenticationMiddleware.hasRole("admin"))
        }

        const middlewaresWithCache = []
        if (this.cacheMiddleware) {
            middlewaresWithCache.push(this.cacheMiddleware.cacheCheck)
        }
        
        this.routes = [
            new Route('get', '/auth/', middlewares, this.handler.list),
            // new Route('get', '/auth/:id', middlewares.concat(middlewaresWithCache), this.handler.show),
            new Route('post', '/auth', middlewares, this.handler.create),
            // new Route('put', '/auth/:id', middlewares, this.handler.update),
            new Route('delete', '/auth/:id', middlewares, this.handler.delete),
            // new Route('post', '/auth/getToken', [], this.handler.getToken),
        ];
    }
}