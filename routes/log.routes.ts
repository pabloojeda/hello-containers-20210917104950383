// import { Route } from '../types';
import { Route } from '../models/route.model'
import { LogHandler } from '../handlers/log.handler';
import { HandlerInterface } from '../framework/base/handler.interface';
import { LogInterface } from '../models/Logs/log.model';
import { BaseRoute, RoutesInterface } from '../framework/base/route.interface';
import { AuthMiddlewareInterface } from '../framework/utils/jwt.middleware.interface';
import { CacheMiddleware } from '../framework/utils/cache.middleware';
export class LogRoutes implements RoutesInterface<LogInterface> {
    handler: HandlerInterface<LogInterface>
    routes: BaseRoute[] = []
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware
    
    constructor(handler: LogHandler, authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        this.handler = handler
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const useMiddlewares = []
        if (this.authenticationMiddleware) {
           useMiddlewares.push(this.authenticationMiddleware.authenticate)
        }

        this.routes = [
            new Route('get', '/logs', useMiddlewares, this.handler.list),
            new Route('get', '/logs/:id', useMiddlewares, this.handler.show),
            new Route('post', '/logs', useMiddlewares, this.handler.create),
            new Route('put', '/logs/:id', useMiddlewares, this.handler.update),
            new Route('delete', '/logs/:id', useMiddlewares, this.handler.delete),
        ];
    }
}