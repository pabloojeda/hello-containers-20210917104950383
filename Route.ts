import { BaseRoute } from './framework/base/route.interface'
export class Routes {
    app: any;
    prefix: string;
    routes: Array<BaseRoute> = []

    constructor(app: any, prefix: string, routes: Array<BaseRoute>) {
        this.app = app
        this.prefix = prefix
        this.routes = routes
        this.createURLRoutes();
    }

    createURLRoutes() {    
        console.log("\nACTIVE PATHS\n======================")
        this.routes.forEach((route) => {
            const { method, path, middleware, handler } = route;
            const pathWithPrefix = `${this.prefix}${path}`
            console.log(method + " - " + pathWithPrefix)
            this.app[method](pathWithPrefix, ...middleware, handler);
        });
    }
}