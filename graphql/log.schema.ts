import { LogController } from "../controllers/log.controller"
import { GraphqlObjectInterface } from "../framework/db/graphql.object.interface"
import { LogInterface } from "../models/Logs/log.model"

export class GraphqlObjectLog implements GraphqlObjectInterface<LogInterface> {
    controller: LogController
    types: string = ""
    inputs: string = ""
    queries: string = ""
    mutations: string = ""
    subscriptions: string = ""
    query: any
    mutation: any
    subscription: any

    constructor(controller: LogController) {
        this.controller = controller
        
        this.defineTypes()
        this.defineInputs()
        this.defineQueries()
        this.defineMutations()
        this.defineSubscriptions()
        this.defineResolvers(controller)
    }

    defineInputs() {
        this.inputs = `
            input LogInput {
                log: String!
                serviceProvider: String
            }
        `;
    } 

    defineTypes() {
        this.types = `
            scalar DateTime
            type Log {
                _id: ID!
                serviceProvider: String
                log: String
                createdAt: DateTime
                updatedAt: DateTime
            }
            type LogChange {
                log: Log,
                operation: String
            }
        `;
    }
 
    defineQueries() {
        this.queries = `
            type Query {
                log(_id: String!): Log,
                logs: [Log]
            }
        `;
    }

    defineMutations() {
        this.mutations = `
            type Mutation {
                createLog(input: LogInput): Log,
                updateLog(_id: ID!, input: LogInput): Log,
                deleteLog(_id: ID!): Log
            }
        `
    }

    defineSubscriptions() {
        this.subscriptions = `
            type Subscription {
                logChange: LogChange
            }
        `
    }

    defineResolvers(controller: LogController) {
        this.query = {
            Query: {
                log: async (root: any, args: any) => {
                    return await controller.show(args._id)
                },
                logs: async () => {
                    const logs = await controller.list(undefined, undefined)
                    return logs.items
                }
            }
        }

        this.mutation = {
            Mutation: {
                createLog: async (root: any, args: any, context: any) => {
                    try {
                        if (!args.input) {
                            throw new Error('Something was wrong creating new Log... Use correct params.')
                        }
                        const log = await controller.create(args.input)
                        if (!log) {
                            throw new Error('Something was wrong creating new Log... Use correct params.')
                        }
                        await context.pubsub.publish('logChange', { logChange: { log: log, operation: "CREATED"} } )

                        return log
                    } catch (error) {
                        console.log(error)
                        throw error
                    }
                },
                updateLog: async (root: any, args: any, context: any) => {
                    try {
                        console.log(args)
                        if (!args._id || !args.input) {
                            throw new Error('Something was wrong updating type. Use correct params...')
                        }
                        const log = await controller.update(args._id, args.input)
                        if (!log) {
                            throw new Error('Something was wrong updating Log... Use correct params.')
                        }

                        await context.pubsub.publish('logChange', { logChange: { log: log, operation: "UPDATED"} } )
    
                        return log
                    } catch (error) {
                        console.log(error)
                        throw error
                    }
                },
                deleteLog: async (root: any, args: any, context: any) => {
                    try {
                        if (!args._id) {
                            throw new Error('Something was wrong updating type...')
                        }
                        const log = await controller.delete(args._id)
                        if (!log) {
                            throw new Error('Something was wrong deleting Log... Use correct params.')
                        }
                        await context.pubsub.publish('logChange', { logChange: { log: log, operation: "DELETED"} } )

                        return log
                    } catch (error) {
                        console.log(error)
                        throw error
                    }
                },
            }
        }

        this.subscription = {
            Subscription: {
                logChange: {
                    subscribe: (root: any, args: any, context: any) => {
                        return context.pubsub.asyncIterator("logChange")
                    }
                }
            }
        }
    }
    
    getGraphQLValues() {
        return {
            types: this.types, 
            inputs: this.inputs, 
            queries: this.queries, 
            mutations: this.mutations,
            subscriptions: this.subscriptions,
            query: this.query,
            mutation: this.mutation,
            subscription: this.subscription
        }
    }
}

