const ws = require('ws'); // yarn add ws
const { createClient } = require('graphql-ws');

const client = createClient({
  url: 'ws://localhost:7080/api/v1/graphql',
  webSocketImpl: ws
});

// query
async function query() {
  console.log("QUERY")
  const result = await new Promise((resolve, reject) => {
    let result;
    client.subscribe(
      {
        query: `{ 
            logs { 
                log
                serviceProvider
            } 
        }`,
      },
      {
        next: (data) => {
            console.log("query next")
            console.log(data)
            (result = data)
        },
        error: (error) => {
            console.log("query ERROR")
            console.log(error)
            reject(error)
        },
        complete: () => {
            console.log("query complete")
            resolve(result)
        }
      },
    );
  });
  console.log("QUERY RESULT")
  console.log(result)
  //expect(result).toEqual({ hello: 'Hello World!' });
};

async function mutation() {
    const result = await new Promise((resolve, reject) => {
      let result;
      client.subscribe(
        {
          query: 'mutation { setHello(msg: "Nuevo hello") }',
        },
        {
          next: (data) => {
              console.log("mutation next")
              console.log(data)
              (result = data)
          },
          error: (error) => {
              console.log("mutation ERROR")
              console.log(error)
              reject(error)
          },
          complete: () => {
              console.log("mutation complete")
              resolve(result)
          }
        },
      );
    });
    console.log("MUTATION RESULT")
    console.log(result)
    //expect(result).toEqual({ hello: 'Hello World!' });
  };

// subscription
// async function subscription() {
//   const onNext = () => {
//     /* handle incoming values */
//     console.log("subsctiption next")
//   };

//   let unsubscribe = () => {
//     /* complete the subscription */
//     // console.log("subsctiption unsubscribe")
//   };

//   await new Promise((resolve, reject) => {
//     unsubscribe = client.subscribe(
//       {
//         query: 'subscription { changeHello }',
//       },
//       {
//         next: (data) => {
//             console.log("subscription next")
//             console.log(data)
//             // onNext
//         },
//         error: (error) => {
//             console.log("subscription error")
//             // console.log(error)
//             reject(error)
//         },
//         complete:(result) => {
//             console.log("subscription complete")
//             console.log(result)
//             resolve(result)
//         },
//       },
//     );
//   });

//   // expect(onNext).toBeCalledTimes(5); // we say "Hi" in 5 languages
// };

// query()
// mutation()
// query()
try {
    let result
    client.subscribe(
        {
          query: `subscription { 
              crypChange {
                cryp {
                  _id
                  iv
                },
                operation
              }
            }`,
        },
        {
          next: (data) => {
              result = data
            console.log('data subscription', JSON.stringify(data));
          },
          error: (error) => {
            console.error('error subscription', error);
          },
          complete: () => {
            console.log('complete subscriptions', result);
          },
        }
      );
} catch (error) {
    console.log(error)
}

// subscription()