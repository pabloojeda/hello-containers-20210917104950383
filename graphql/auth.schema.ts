import { AuthController } from "../controllers/auth.controller.redis"
import { ControllerInterface } from "../framework/base/controller.interface"
import { GraphqlObjectInterface } from "../framework/db/graphql.object.interface"
import { JWTManager } from "../framework/utils/jwt.manager"
import { AuthInterface } from "../models/Auth/auth.model"

export class GraphqlObjectAuth implements GraphqlObjectInterface<AuthInterface> {
    controller: ControllerInterface<AuthInterface>
    jwtManager: JWTManager;
    types: string = ""
    inputs: string = ""
    queries: string = ""
    mutations: string = ""
    subscriptions: string = ""
    query: any
    mutation: any
    subscription: any

    constructor(controller: ControllerInterface<AuthInterface>, jwtManager: JWTManager) {
        this.controller = controller
        this.jwtManager = jwtManager

        this.defineTypes()
        this.defineInputs()
        this.defineQueries()
        this.defineMutations()
        this.defineSubscriptions()
        this.defineResolvers(controller as AuthController)
    }

    defineInputs() {
        this.inputs = `
            input AuthInput {
                apiKey: String!
                user: String!
            }
        `;
    } 

    defineTypes() {
        this.types = `
            type Auth {
                _id: String
                apiKey: String
                user: String
                expiredAt: DateTime
                createdAt: DateTime
                updatedAt: DateTime
            }

            type Token {
                token: String,
                expired: Float
            }
        `;
    }
 
    defineQueries() { }

    defineMutations() {
        this.mutations = `
            type Mutation {
                getToken(input: AuthInput): Token
            }
        `
    }

    defineSubscriptions() { }

    defineResolvers(controller: AuthController) {
        this.query = { }

        this.mutation = {
            Mutation: {
                getToken: async (root: any, args: any, context: any) => {
                    try {
                        if (!args.input) {
                            throw new Error('Something was wrong creating new Auth... Use correct params.')
                        }
                        const { user, apiKey } = args.input
                        if (!user || !apiKey) {
                            return new Error('Insuficient params')
                        }
                        const auth = await this.controller.show(args.input)
                        if (!auth) {
                            throw new Error('Something was wrong creating new Auth... Use correct params.')
                        }
                        if (!auth.apiKey || (auth.apiKey && auth.apiKey !== apiKey) ) {
                            return new Error('Not authorized. Something wrong with your credentials')
                        }
                        if (!process.env.JWT_TOKEN) {
                            throw new Error("NOT TOKEN DEFINED")
                        }
            
                        //const jwtManager = new JWTManager(process.env.JWT_TOKEN, this.ttl)
                        const token = await this.jwtManager.generateAccessToken(auth)
                        const dateNow = new Date() 
                        return auth
                    } catch (error) {
                        console.log(error)
                        throw error
                    }
                }
            }
        }

        this.subscription = { }
    }
    
    getGraphQLValues() {
        return {
            types: this.types, 
            inputs: this.inputs, 
            queries: this.queries, 
            mutations: this.mutations,
            subscriptions: this.subscriptions,
            query: this.query,
            mutation: this.mutation,
            subscription: this.subscription
        }
    }
}

