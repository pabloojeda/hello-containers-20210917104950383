import { GraphqlObjectMicroservice } from "../framework/db/graphql.object.interface"
import { httpRequest } from "../framework/utils/fetch.manager"

export class GraphqlObjectCryp implements GraphqlObjectMicroservice {
    path: string
    types: string = ""
    inputs: string = ""
    queries: string = ""
    mutations: string = ""
    subscriptions: string = ""
    query: any
    mutation: any
    subscription: any

    constructor(path: string) {
        this.path = path
        
        this.defineTypes()
        this.defineInputs()
        this.defineQueries()
        this.defineMutations()
        this.defineSubscriptions()
        this.defineResolvers()
    }

    defineInputs() {
        this.inputs = ``;
    } 

    defineTypes() {
        this.types = `
            scalar DateTime
            type Cryp {
                _id: ID!
                sharedSecret: String
                iv: String
                user: String
                createdAt: DateTime
                updatedAt: DateTime
            }
            type CrypChange {
                cryp: Cryp,
                operation: String
            }
        `;
    }
 
    defineQueries() {
        this.queries = `
            type Query {
                cryp(_id: String!): Cryp
            }
        `;
    }

    defineMutations() {
        this.mutations = ``
    }

    defineSubscriptions() {
        this.subscriptions = `
            type Subscription {
                crypChange: CrypChange
            }
        `
    }

    defineResolvers() {
        this.query = {
            Query: {
                cryp: async (root: any, args: any, context: any) => {
                    const url = `${this.path}/cryp/${args._id}`
                    try {
                        const response = await httpRequest(url, context.req)
                        return response
                    } catch (error) {
                        throw error
                    }
                }
            }
        }

        this.mutation = {
        }

        this.subscription = {
            Subscription: {
                crypChange: {
                    subscribe: (root: any, args: any, context: any) => {
                        return context.pubsub.asyncIterator("crypChange")
                    }
                }
            }
        }
    }
    
    getGraphQLValues() {
        return {
            types: this.types, 
            inputs: this.inputs, 
            queries: this.queries, 
            mutations: this.mutations,
            subscriptions: this.subscriptions,
            query: this.query,
            mutation: this.mutation,
            subscription: this.subscription
        }
    }
}

