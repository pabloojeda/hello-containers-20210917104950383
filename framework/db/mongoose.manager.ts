import { Mongoose } from "mongoose";

const mongoose = require('mongoose');
let mongoUri = process.env.MONGO_URI || undefined

import fs from "fs"
import path from "path"

let ca : any
try {
    if (process.env.ENV === "DEV") {
        ca = fs.readFileSync(`${path.join(__dirname, "../../config/fae6af91-1aff-457b-bef1-9ca2af12d4c7.pem")}`)
    }
} catch (error) {
    console.log(error)
    throw new Error("there was a problem reading mongo cert");
}

import util from "util"
import assert from "assert"

export default class MongooseManager {

    constructor() {
        // if (!mongoUri) {
        //     throw new Error("Not mongo uri finded");
        // }

        let credentials;
        if (process.env.ENV !== "DEV") {
            if (process.env.BINDING) {
                credentials = JSON.parse(process.env.BINDING);
            }
    
            assert(!util.isUndefined(credentials), "Must be bound to IBM Kubernetes Cluster");

            // We now take the first bound MongoDB service and extract its credentials object from BINDING
            let mongodbConn = credentials.connection.mongodb;

            // Read the CA certificate and assign that to the CA variable
            ca = [Buffer.from(mongodbConn.certificate.certificate_base64, 'base64')];

            // Extract the database username and password
            let authentication = mongodbConn.authentication;
            let username = authentication.username;
            let password = authentication.password;
            let mongoDbName = process.env.MONGO_DB_NAME

            // Extract the MongoDB URIs
            let connectionPath = mongodbConn.hosts;
            let connectionString = `mongodb://${username}:${password}@${connectionPath[0].hostname}:${connectionPath[0].port},${connectionPath[1].hostname}:${connectionPath[1].port}/${mongoDbName}?replicaSet=replset`;
            console.log(connectionString)

            mongoUri = connectionString
        } else {
            let mongoUsername = process.env.MONGO_DB_USER
            let mongoPass = process.env.MONGO_DB_PASS
            let mongoHostPort = process.env.MONGO_DB_HOST_PORT
            let mongoDbName = process.env.MONGO_DB_NAME

            if (!mongoUsername || mongoUsername === "" ||
                !mongoPass || mongoPass === "" ||
                !mongoHostPort || mongoHostPort === "" ||
                !mongoDbName || mongoDbName === "" ||
                !ca) {
                    throw new Error("Mongo ENV params are not defined");
                }

            mongoUri = `mongodb://${mongoUsername}:${mongoPass}@${mongoHostPort}/${mongoDbName}?replicaSet=replset`
        }
    }

    async connectMongo() {
        if (!ca) {
            throw new Error("Trying to connect mongo but ssl ca certificate is not detected");
        }

        try {
            mongoose.connect(mongoUri, {
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                ssl: true,
                sslValidate: true,
                sslCA: ca,
                auth:{
                    "authSource": "admin"
                }
            });
            
            mongoose.connection.on('error', (error: Error) => {
                console.log('DB connection failed. Will try again.');
                console.log(error)
            });
    
            mongoose.connection.on('connected', function () {
                console.log("\nMONGO CONNECTED: " + mongoUri)
            });
            
            mongoose.connection.on('disconnected', function () {
                console.log('Desconectado de la base de datos')
            })
            
            await mongoose.connection.once("open", function() {
                console.log("MongoDB database connection established successfully\n======================");
            })
            
            process.on('SIGINT', function() {
                mongoose.connection.close(function () {
                    console.log('Desconectado de la base de datos al terminar la app')
                    process.exit(0)
                })
            })
        } catch (error) {
            console.log("MONGO ERROR")
            console.log(error)
        }
    }

    getMongoose(): Mongoose {
        return mongoose;
    }
}