import { PaginationResponseInterface } from "../utils/pagination.interface";
import fs from "fs"
import path from "path"
import util from "util"
import assert from "assert"

const redis = require('redis')
const { promisify } = require('util');
const REDIS_HOST = process.env.REDIS_HOST || undefined // "redis-api-dev://root:Insulcl0ck69@localhost:6379/1"
const REDIS_HOST_PUBSUB = process.env.REDIS_HOST_PUBSUB
const REDIS_CACHE_TIMEOUT = Number(process.env.REDIS_CACHE_TIMEOUT) || 120

let ca : any = undefined
try {
    if (process.env.ENV === "DEV") {
        ca = fs.readFileSync(`${path.join(__dirname, "../../config/fae6af91-1aff-457b-bef1-9ca2af12d4c7.pem")}`)
    }
} catch (error) {
    console.log(error)
    throw new Error("there was a problem reading redis cert");
}

import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Redis } from "ioredis";
export class RedisManager {
    private static instance: RedisManager;

    client = this.configureRedis()
    clientPublisher? : RedisManager
    clientSubscriber? : RedisManager
    redisPubsub?: RedisPubSub
    timeout: number = Number(REDIS_CACHE_TIMEOUT)

    constructor() {
        if (this.client) {
            this.client.on("error", (error: Error) => {
                console.error(`\n ==>>> ERROR to connect Redis: ${error} \n======================`);
            });
            
            const r = this.client
            const pass = process.env.REDIS_PASS ? process.env.REDIS_PASS : "146f68c98aa01152ea3beb1a26ceee96b7485238ff4b47f53d0"
            this.client.on('ready',function() {
                console.log(`\n Redis connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds \n======================`);
                // r.auth(pass)
            });
        }
        
    }

    configureRedis(): Redis {
        let credentials;
        if (process.env.ENV !== "DEV") {
            if (process.env.BINDING_REDIS) {
                credentials = JSON.parse(process.env.BINDING_REDIS);
            }
    
            assert(!util.isUndefined(credentials), "Must be bound to IBM Kubernetes Cluster");

            let redisconn = credentials.connection.rediss;
            console.log("REDIS")
            console.log(redisconn)
            let connectionString = redisconn.composed[0];
            console.log(RTCPeerConnectionIceErrorEvent)
            let caCert = [Buffer.from(redisconn.certificate.certificate_base64, 'base64')]
            console.log(caCert)
            console.log(new URL(connectionString).hostname)
            return redis.createClient(connectionString, {
                    tls: { ca: caCert, servername: new URL(connectionString).hostname }
            });

        } else {
            if (!REDIS_HOST || !ca) {
                assert(!util.isUndefined(credentials), "REDIS HOST not defined");
            }
            let cert = fs.readFileSync(`${path.join(__dirname, "../../config/fae6af91-1aff-457b-bef1-9ca2af12d4c7.pem")}`, { encoding: 'ascii' })
            //let cert = Buffer.from(`${path.join(__dirname, "../../config/fae6af91-1aff-457b-bef1-9ca2af12d4c7")}`, 'base64').toString();
            var r = redis.createClient(
                // REDIS_HOST,
            {
                // host: "b8bd7720-b79d-45bc-977e-0ef223c20569.br38q28f0334iom5lv4g.private.databases.appdomain.cloud",
                // port: "32145",
                // user: "admin",
                password: "146f68c98aa01152ea3beb1a26ceee96b7485238ff4b47f53d0",
                url: REDIS_HOST, // "rediss://b8bd7720-b79d-45bc-977e-0ef223c20569.br38q28f0334iom5lv4g.private.databases.appdomain.cloud:32145/0",
                // no_ready_check: true,
                tls: {
                    ca: cert,
                    servername: "b8bd7720-b79d-45bc-977e-0ef223c20569.br38q28f0334iom5lv4g.private.databases.appdomain.cloud",
                    
                    // host: "b8bd7720-b79d-45bc-977e-0ef223c20569.br38q28f0334iom5lv4g.private.databases.appdomain.cloud",
                    // port: 32145, 
                }
            })
            // r.auth(process.env.REDIS_PASS)
            return r
        }
    }

    public static getInstance(): RedisManager {
        if (!RedisManager.instance) {
            RedisManager.instance = new RedisManager();
        }

        return RedisManager.instance;
    }

    public configureRedisPubSub() {
        this.clientPublisher = this.getRedisClientPublisher()
        this.clientSubscriber = this.getRedisClientSubscriber()
        this.redisPubsub = new RedisPubSub({
            publisher: this.clientPublisher.client,
            subscriber: this.clientSubscriber.client
        })
    }

    public getRedisClientPublisher(): RedisManager {
        if (!REDIS_HOST_PUBSUB || REDIS_HOST_PUBSUB === "") {
            throw new Error("Can not connect to redis pubsub because we don't have configuration params")
        }
        if (this.clientPublisher !== undefined) {
            return this.clientPublisher
        }
        const redisManagerPublisher = new RedisManager()
        const clientPublisher = redis.createClient( { url: REDIS_HOST_PUBSUB} );
        redisManagerPublisher.client = clientPublisher

        clientPublisher.on("error", (error: Error) => {
            console.error(`\n ==>>> ERROR to connect Redis as Publisher: ${error}\N======================`);
        });

        clientPublisher.on('ready',function() {
            console.log(`\nRedis Publisher (${REDIS_HOST_PUBSUB}) connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds\n======================`);
        });

        return redisManagerPublisher
    }

    public getRedisClientSubscriber(): RedisManager {
        if (!REDIS_HOST_PUBSUB || REDIS_HOST_PUBSUB === "") {
            throw new Error("Can not connect to redis pubsub because we don't have configuration params")
        }
        if (this.clientSubscriber !== undefined) {
            return this.clientSubscriber
        }
        const redisManagerSubscriber = new RedisManager()
        const clientSubscriber = redis.createClient( { url: REDIS_HOST_PUBSUB} );
        redisManagerSubscriber.client = clientSubscriber

        clientSubscriber.on("error", (error: Error) => {
            console.error(`\n ==>>> ERROR to connect Redis as Subscriber: ${error}\N======================`);
        });

        clientSubscriber.on('ready',function() {
            console.log(`\nRedis Subscriber (${REDIS_HOST_PUBSUB}) connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds\n======================`);
        });

        return redisManagerSubscriber
    }

    // async saveArray(key:string, array: any[], pagination: PaginationResponseInterface | undefined = undefined, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
    async saveArray(key:string, array: any[], pagination: PaginationResponseInterface | undefined = undefined, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
        try {
            await this.delAsync(key)
            for (let i = 0; i < array.length; i++) {
                const item = array[i]
                const objectKey = `${key.split(":")[0]}:${item._id}`
                await this.hmsetAsync(objectKey, item)
                await this.expireAsync(objectKey, expire)
                await this.lpushAsync(key, objectKey)
            }
            await this.expireAsync(key, expire)
            if (pagination) {
                await this.hmsetAsync(`${key}:pagination`, pagination)
                await this.expireAsync(`${key}:pagination`, expire)
            }
            return Promise.resolve(true)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async getArray<T>(key: string): Promise<{items: T[], pagination: PaginationResponseInterface}> {
        const keys = await this.lrangeAsync(key, 0, -1) // 0, -1 => all items
        // const sort = await this.client.sort(key)
        // sort listLogs by nosort get *->_id get *->serviceProvider
        const items = []
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const log = await this.hgetallAsync(key)
            items.push(log)
        }
        const pagination = await this.hgetallAsync(`${key}:pagination`)
        return {items: items, pagination: pagination}
    }

    async saveObject(key: string, item: any, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
        try {
            const set = await this.hmsetAsync(`${key}:${item._id}`, item)
            await this.expireAsync(`${key}:${item._id}`, expire)
            if (set)  {
                return Promise.resolve(true)
            }
            return Promise.resolve(false)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async saveObjectAndArray(key: string, item: any): Promise<Boolean> {
        try {
            const itemSaved = await this.hmsetAsync(`${key}:${item._id}`, item)
            if (itemSaved) {
                const objectKey = `${key}:${item._id}`
                const listKey = key+":list"
                await this.lremAsync(listKey, 1, objectKey)
                await this.lpushAsync(listKey, objectKey)
                return Promise.resolve(true)
            }
            return Promise.resolve(false)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async getObject<T>(key: string): Promise<T> {
        const item = await this.hgetallAsync(key)
        return item
    }

    async deleteObjectAndUpdateArray(objectKey: string, key: string) {
        try {
            await this.delAsync(objectKey)
            await this.lremAsync(key+":list", 1, objectKey)
        } catch (error) {
            throw new Error("Delete object error.");
        }
    }

    async clearCache(key: string) {
        try {
            // Delete simple key
            const delKey = await this.delAsync(key)
            if (!delKey) {
                // Delete * list keys
                const keys = await this.keysAsync(key)
                if (keys) {
                    for (let i = 0; i < keys.length; i++) {
                        const key = keys[i];
                        await this.delAsync(key)
                    }
                }
            }
        } catch (error) {
            throw new Error("Clear caché error.");
        }
    }

    getListCacheKey(path: string, query: string[]) {
        let key = path + ":list:"
        query.forEach(k => {
            key = key + "_" + k
        })
        return key
    }

    getObjectCacheKey(path: string, id: string) {
        let key = path + ":" + id
        return key
    }

    expireAsync = promisify(this.client.expire).bind(this.client)
    getAsync    = promisify(this.client.get).bind(this.client)
    setAsync    = promisify(this.client.set).bind(this.client)
    delAsync    = promisify(this.client.del).bind(this.client)
    hgetallAsync= promisify(this.client.hgetall).bind(this.client)
    hmsetAsync  = promisify(this.client.hmset).bind(this.client)
    setexAsync  = promisify(this.client.setex).bind(this.client)
    keysAsync   = promisify(this.client.keys).bind(this.client)
    lpushAsync  = promisify(this.client.lpush).bind(this.client)
    lrangeAsync = promisify(this.client.lrange).bind(this.client)
    lremAsync   = promisify(this.client.lrem).bind(this.client)
    auth        = promisify(this.client.auth).bind(this.client)
}


export const redisManager = RedisManager.getInstance()