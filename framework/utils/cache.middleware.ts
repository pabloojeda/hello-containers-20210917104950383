import { RedisManager } from "../db/redis.manager";
import { RequestHandler as Middleware } from 'express';
import { NextFunction, Request, Response } from 'express';
import { Console } from "node:console";

export class CacheMiddleware {
    app: any
    client: RedisManager

    constructor(app: any, client: RedisManager) {
        this.app = app
        this.client = client
    }

    cacheCheck: Middleware = async (req: Request, res: Response, next: NextFunction) => {
        const key = this.generateCacheKey(req)
        const data = await this.client.getAsync(key)

        if (data) {
            if (process.env.ENV && process.env.ENV === "DEV") {
                console.log(`Response is send from cache with key: ${key}`);
            }
            const jsonData = JSON.parse(data)
            jsonData.cache = true
            return res.status(200).send(jsonData);
        }
        next()
    }

    cacheSave: Middleware = async (req: Request, res: any, next: NextFunction) => {
        if (req.method !== "GET") {
            return next()
        }

        if (req.path.includes("graphql")) {
            return next()
        }

        let oldSend = res.send
        let resData : any = undefined
        res.send = function(data: any) {
            resData = data
            res.send = oldSend // set function back to avoid the 'double-send'
            return res.send(data) // just call as normal with data
            // oldSend.call(this, data) // other posibility
        }

        const generateCacheKey = this.generateCacheKey
        const redisClient = this.client

        res.on("finish", async function() {
            if (req.method === "GET" && res.statusCode && res.statusCode < 300 && resData && (!resData.cache || resData.cache !== true)) {
                const key = generateCacheKey(req)
                if (process.env.ENV && process.env.ENV === "DEV") {
                    console.log(`Response is saving to cache with key: ${key}`);
                    await redisClient.setexAsync(key, redisClient.timeout, JSON.stringify(resData));
                    console.log(resData)
                }
            }
        });
        next()
    }

    generateCacheKey(req: Request) {
        const path = req.path
        return `cache:${path}`;
    }
}