export enum ErrorType {
    BadRequestCredentials,
    BadRequestParams,
    BadRequestRole,
    ResourceNotFound,
    Unauthorized,
    UnauthorizedApiKey,
    Generic
}

export function getErrorJSON(errorType: ErrorType): {[key: string]: any} {
    switch (errorType) {
        case ErrorType.BadRequestCredentials:
            return { error: "Bad Request", error_code: 0 }
        case ErrorType.BadRequestParams:
            return { error: "Bad Params", error_code: 1 }
        case ErrorType.BadRequestRole:
            return { error: "Bad Request", error_code: 2 }
        case ErrorType.ResourceNotFound:
            return { error: "Resource not found", error_code: 0 }
        case ErrorType.Unauthorized:
            return { error: "Unauthorized", error_code: 0 }
        case ErrorType.UnauthorizedApiKey:
                return { error: "Unauthorized", error_code: 1 }
        case ErrorType.Generic:
            return { error: "Something was grong", error_code: 99 }
        default:
            return { error: "Something was grong", error_code: 100 }
    }
}