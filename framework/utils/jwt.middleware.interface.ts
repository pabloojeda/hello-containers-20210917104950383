import { RequestHandler as Middleware } from 'express';
import { JWTManagerInterface } from './jwt.manager';

export interface AuthMiddlewareInterface {
    authenticate: Middleware
    hasRole(role: string): Middleware
    
    jwt: JWTManagerInterface
}