export interface Identifiable {
    _id: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface JSONInterface extends Identifiable {
    toJSON(): { [key: string]: string }
}

export interface ShortFieldsInterface<T> extends Identifiable {
    shortFields(): T
}