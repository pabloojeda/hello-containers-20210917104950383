import { Identifiable } from "./basemodel.interface";
import { ManagerInterface } from "./manager.interface";
import { PaginationInterface, PaginationResponseInterface } from "../utils/pagination.interface";
import { RedisManager } from "../db/redis.manager";
import { SearchFieldsInterface } from "../utils/searchField.interface";
import { CryptoManagerInterface } from "../utils/crypto.manager";

export interface ControllerInterface<T extends Identifiable> {
    dataBase: ManagerInterface<any, T>
    encryptionManager: CryptoManagerInterface | undefined
    cacheDataBase: RedisManager | undefined
    
    list(searchFields: SearchFieldsInterface | undefined, pagination: PaginationInterface | undefined): Promise<{items: T[], pagination: PaginationResponseInterface | undefined}>
    show(id: string): Promise<T | undefined>
    create(item: T): Promise<T>
    update(id: string, item: T): Promise<T | undefined>
    delete(id: string): Promise<T | undefined>
}