import { ManagerInterface } from "./manager.interface";
import { RedisManager } from "../db/redis.manager";
import { Identifiable } from "./basemodel.interface";
import { ControllerInterface } from "./controller.interface";
import { HandlerInterface, HandlerReadInterface } from "./handler.interface";
import { RoutesInterface, RoutesReadInterface } from "./route.interface";
import { CryptoManagerInterface } from "../utils/crypto.manager";

// const logRoutes = new LogRoutes(new LogHandler(new LogController(new LogDBDirector(new LogDBBuilder(), this.redisManager))))
export interface Builder<U, T extends Identifiable> {
    routes?: RoutesInterface<T> | RoutesReadInterface<T>
    handler?: HandlerInterface<T> | HandlerReadInterface<T>
    controller?: ControllerInterface<T>
    database?: ManagerInterface<U, T>
    cacheDataBase?: RedisManager
    cache: Boolean
    encryptionManager?: CryptoManagerInterface
    
    setRoutes(routes: RoutesInterface<T> | RoutesReadInterface<T>): Builder<U, T>;
    setHandler(handler: HandlerInterface<T> | HandlerReadInterface<T>): Builder<U, T>;
    setController(controller: ControllerInterface<T>): Builder<U, T>
    setDatabase(db: ManagerInterface<U, T>): Builder<U, T>;
    setCacheDB(redisManager: RedisManager | undefined): Builder<U, T>;
    setCache(cache: Boolean): Builder<U, T>;
    setEncryption(encryptionManager: CryptoManagerInterface): Builder<U, T>;

    getRoutes(): RoutesInterface<T> | RoutesReadInterface<T> | undefined
    getHandler(): HandlerInterface<T> | HandlerReadInterface<T> | undefined
    getController(): ControllerInterface<T> | undefined
    getDatabase(): ManagerInterface<U, T> | undefined
    getCacheDB(): RedisManager | undefined;
    getCache(): Boolean;
    getEncryption(): CryptoManagerInterface | undefined;

    build(): Builder<U, T>;
}