import { Handler } from "express";
import { ControllerInterface } from "./controller.interface";
import { Identifiable } from "./basemodel.interface";
import { JWTManager } from "../utils/jwt.manager";

export interface HandlerInterface<T extends Identifiable> {
    controller: ControllerInterface<T>
    
    list: Handler
    show: Handler
    create: Handler
    update: Handler
    delete: Handler
}

export interface HandlerReadInterface<T extends Identifiable> {
    controller: ControllerInterface<T>
    
    list: Handler
    show: Handler
}

export interface HandlerWriteInterface<T extends Identifiable> {
    controller: ControllerInterface<T>

    create: Handler
    update: Handler
    delete: Handler
}

export interface HandlerAuthInterface<T extends Identifiable> {
    controller: ControllerInterface<T>
    jwtManager: JWTManager
    
    list: Handler
    show: Handler
    create: Handler
    update: Handler
    delete: Handler
}