
import { ManagerInterface } from '../../framework/base/manager.interface';
import { BearerInterface } from "../../models/Bearer/bearer.model";
import { PaginationInterface, PaginationResponse, PaginationResponseInterface } from '../../framework/utils/pagination.interface';
import { SearchFieldsInterface } from '../../framework/utils/searchField.interface';
import MongooseManager from '../../framework/db/mongoose.manager';
import { AuthInterface } from '../../models/Auth/auth.model';

export class BearerManagerMongo implements ManagerInterface<MongooseManager, BearerInterface> {
    database: MongooseManager

    constructor(database: MongooseManager) {
        this.database = database
    }

    async list(searchFields: SearchFieldsInterface | undefined, pagination: PaginationInterface | undefined): Promise<{items: BearerInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const limit = pagination && pagination.limit ? pagination.limit : 100
            const page = pagination && pagination.page ? pagination.page : 1
            
            const query = this.database.getMongoose().models.Bearer.find()
            const totalBearer = await this.database.getMongoose().models.Bearer.countDocuments()

            query.limit(limit * 1).skip((page - 1) * limit).sort({ createdAt: -1 })
            
            const bearers = await query
            const jsonBearers = bearers.map(b => {
                return b.toJSON()
            })
            
            const paginationResponse = new PaginationResponse(totalBearer, page, limit)
            const response = { items: jsonBearers, pagination: paginationResponse }            
            return response
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async show(token: string): Promise<BearerInterface | undefined> {
        try {
            const bearer = await this.database.getMongoose().models.Bearer.findOne({token: token})
            if(!bearer) {
                return Promise.resolve(undefined)
            }
            return bearer.toJSON()
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async create(item: BearerInterface): Promise<BearerInterface> {
        try {
            const mongoose = this.database.getMongoose()
            var bearer = new mongoose.models.Bearer(item)
            const savedBearer = await bearer.save()
            if (savedBearer) {
                return savedBearer.toJSON()
            }
            return Promise.reject("There was a problems saving Bearer in Mongo")
        } catch (error) {
            return Promise.reject(error)
        }        
    }
    async update(id: string, item: BearerInterface): Promise<BearerInterface | undefined> {
        try {            
            if (!id || !item || !item.expiredAt) {
                return Promise.reject(new Error('Something was wrong updating Bearer... Use correct params.'))
            }
            let params : any = {}
            params.expiredAt = item.expiredAt
            
            let bearer = await this.database.getMongoose().models.Bearer.findOneAndUpdate({_id: id}, { $set: params }, { new: true, runValidators: true })
            if (!bearer) {
                return Promise.reject(new Error('Something was wrong updating Bearer... Can not find the bearer.'))
            }
            
            const bearerJSON = bearer.toJSON()
            return bearerJSON
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async delete(id: string): Promise<BearerInterface | undefined> {
        try {
            const removeBearer = await this.database.getMongoose().models.Bearer.findByIdAndRemove(id)
            if (removeBearer) {
                return removeBearer.toJSON()
            }
            return Promise.reject(new Error('Something was wrong deleting Bearer... Can not find the bearer.'))
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async getUser(id: string): Promise<AuthInterface | undefined> {
        try {
            const auth = await this.database.getMongoose().models.Auth.findOne({userID: id})
            if(!auth) {
                return Promise.resolve(undefined)
            }
            return auth.toJSON()
        } catch (error) {
            return Promise.reject(error)
        }
    }
}