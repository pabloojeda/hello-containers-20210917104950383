
import { ManagerInterface } from '../../framework/base/manager.interface';
import { AuthInterface } from "../../models/Auth/auth.model";
import { PaginationInterface, PaginationResponse, PaginationResponseInterface } from '../../framework/utils/pagination.interface';
import { SearchFieldsInterface } from '../../framework/utils/searchField.interface';
import MongooseManager from '../../framework/db/mongoose.manager';

export class AuthManagerMongo implements ManagerInterface<MongooseManager, AuthInterface> {
    database: MongooseManager

    constructor(database: MongooseManager) {
        this.database = database
    }

    async list(searchFields: SearchFieldsInterface | undefined, pagination: PaginationInterface | undefined): Promise<{items: AuthInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const limit = pagination && pagination.limit ? pagination.limit : 100
            const page = pagination && pagination.page ? pagination.page : 1
            
            const query = this.database.getMongoose().models.Auth.find()
            const totalAuth = await this.database.getMongoose().models.Auth.countDocuments()

            query.limit(limit * 1).skip((page - 1) * limit).sort({ createdAt: -1 })
            
            const auths = await query
            const jsonAuth = auths.map(a => {
                return a.toJSON()
            })
            
            const paginationResponse = new PaginationResponse(totalAuth, page, limit)
            const response = { items: jsonAuth, pagination: paginationResponse }            
            return response
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async show(id: string): Promise<AuthInterface | undefined> {
        try {
            const auth = await this.database.getMongoose().models.Auth.findOne({userID: id})
            if(!auth) {
                return Promise.resolve(undefined)
            }
            return auth.toJSON()
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async create(item: AuthInterface): Promise<AuthInterface> {
        try {
            const mongoose = this.database.getMongoose()
            var auth = new mongoose.models.Auth(item)
            const savedAuth = await auth.save()
            if (savedAuth) {
                return savedAuth.toJSON()
            }
            return Promise.reject("There was a problems saving Auth in Mongo")
        } catch (error) {
            return Promise.reject(error)
        }        
    }
    async update(id: string, item: AuthInterface): Promise<AuthInterface | undefined> {
        // try {            
        //     if (!id || !item || !item.expiredAt) {
        //         return Promise.reject(new Error('Something was wrong updating Auth... Use correct params.'))
        //     }
        //     let params : any = {}
        //     params.expiredAt = item.expiredAt
            
        //     let auth = await this.database.getMongoose().models.Autn.findOneAndUpdate({userID: id}, { $set: params }, { new: true, runValidators: true })
        //     if (!auth) {
        //         return Promise.reject(new Error('Something was wrong updating Auth... Can not find the bearer.'))
        //     }
            
        //     const authJSON = auth.toJSON()
        //     return authJSON
        // } catch (error) {
        //     return Promise.reject(error)
        // }
        throw new Error("Method not implemented")
    }
    async delete(id: string): Promise<AuthInterface | undefined> {
        try {
            const removeAuth = await this.database.getMongoose().models.Auth.findByIdAndRemove(id)
            if (removeAuth) {
                return removeAuth.toJSON()
            }
            return Promise.reject(new Error('Something was wrong deleting Auth... Can not find the auth.'))
        } catch (error) {
            return Promise.reject(error)
        }
    }
    
}