import { PaginationResponseInterface } from "../../framework/utils/pagination.interface";
import { ManagerInterface } from "../../framework/base/manager.interface";
import { Auth, AuthInterface, Role, getRole, getApiKey } from "../../models/Auth/auth.model";
import { RedisManager } from "../../framework/db/redis.manager";
import { createUUID } from "../../framework/utils/utils"

const AUTH_TTL : number = Number(process.env.AUTH_TTL) || 24*60*60
export class AuthManagerRedis implements ManagerInterface<RedisManager, AuthInterface>{
    database: RedisManager
    ttl: number

    constructor(database: any, ttl: number = AUTH_TTL) {
        this.database = database
        this.ttl = ttl
    }

    async list(): Promise<{items: AuthInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const key = "auth:list"
            const data = await this.database.getArray<AuthInterface>(key)
            if (data && data.items) {
                const items = data.items
                return Promise.resolve({items: items, pagination: undefined})
            }
            return Promise.resolve({items: [], pagination: undefined})
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async show(id: string): Promise<AuthInterface | undefined> {
        const key = this.database.getObjectCacheKey("auth", id)
        const authToShow = await this.database.getObject(key)
        if (authToShow) {
            const auth = authToShow as Auth
            const item = new Auth(auth._id, auth.apiKey, auth.role)
            return Promise.resolve(item.shortFields());
        }
        return Promise.resolve(undefined)
    }

    async create(item: AuthInterface): Promise<AuthInterface> {
        const id : string = item._id ? item._id : createUUID()
        const key = this.database.getObjectCacheKey("auth", id)
        const authToShow = await this.database.getObject(key)
        if (authToShow) {
            return Promise.reject(new Error("There was a problem creating Auth object. The object exist."));
        }
        const role : Role = getRole(item.role || "")
        const auth = new Auth(id, getApiKey(), role)
        const itemJSON = auth.toJSON()
        const created = await this.database.saveObjectAndArray("auth", itemJSON)
        return created ? Promise.resolve(auth) : Promise.reject("There was a problem creating Auth object")
    }

    async update(id: string, item: AuthInterface): Promise<AuthInterface | undefined> {
        const key = this.database.getObjectCacheKey("auth", id)
        const auth = (await this.database.getObject(key)) as {[key: string]:string}
        if (auth) {
            const itemAuth = new Auth(auth._id, auth.apiKey, getRole(auth.role || ""), auth.expiredAt, new Date(auth.createdAt), new Date(auth.updatedAt))
            if (item.role) {
                itemAuth.role = getRole(item.role)
            }
            if (item.apiKey) {
                itemAuth.apiKey = item.apiKey
            }
            itemAuth.updatedAt = new Date()
            const authJSON = itemAuth.toJSON()
            const updated = await this.database.saveObjectAndArray("auth", authJSON)
            return updated ? Promise.resolve(itemAuth) : Promise.reject("There was a problem updating Auth object")
        }
        return undefined
    }
    
    async delete(id: string): Promise<AuthInterface | undefined> {
        const key = this.database.getObjectCacheKey("auth", id)
        const authToShow = await this.database.getObject(key)
        if (authToShow) {
            const auth = authToShow as Auth
            const item = new Auth(auth._id, auth.apiKey, auth.role)
            await this.database.deleteObjectAndUpdateArray(key, "auth")
            return Promise.resolve(item.shortFields());
        }
        return Promise.reject("There was a problem deleting Auth object. Not resource founded.")
    }
}