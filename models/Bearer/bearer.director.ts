import { RedisManager } from "../../framework/db/redis.manager";
import { Builder } from "../../framework/base/builder.interface";
import { DirectorInterface, DirectorAuthInterface, DirectorInterfaceRedis, DirectorInterfaceMongo } from "../../framework/base/director.interface";
import { BearerInterface } from "./bearer.model";
import { BaseRoute } from "../../framework/base/route.interface";
import { BearerDBBuilder } from "./bearer.builder";
import { BearerManagerMongo } from "../../managers/Bearer/bearer.manager.mongo";
import { BearerController } from "../../controllers/bearer.controller";
import { BearerHandler } from "../../handlers/bearer.handler";
import { BearerRoutes } from "../../routes/bearer.routes";
import { AuthMiddlewareInterface } from "../../framework/utils/jwt.middleware.interface";
import { JWTManager } from "../../framework/utils/jwt.manager";
import { CacheMiddleware } from "../../framework/utils/cache.middleware";
import MongooseManager from "../../framework/db/mongoose.manager";
import { CryptoManager } from "../../framework/utils/crypto.manager";

export class BearerDBDirector implements DirectorInterface<MongooseManager, BearerInterface>, DirectorAuthInterface<MongooseManager, BearerInterface>, DirectorInterfaceMongo<MongooseManager, BearerInterface> {
    builder: Builder<MongooseManager, BearerInterface> = new BearerDBBuilder()
    cacheDB?: RedisManager
    database: MongooseManager
    authenticationMiddleware?: AuthMiddlewareInterface
    cacheMiddleware?: CacheMiddleware
    jwtManager?: JWTManager
    
    constructor(database: MongooseManager, cacheDB: RedisManager | undefined = undefined, authenticationMiddleware?: AuthMiddlewareInterface, cacheMiddleware?: CacheMiddleware, jwtManager?: JWTManager) {
        this.database = database
        this.cacheDB = cacheDB
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.jwtManager = jwtManager
    }
    
    buildMongo(cache: string = "False"): void {
        if (!this.jwtManager) {
            throw new Error("JWTManager is not provided");   
        }

        const bearerDataBase = new BearerManagerMongo(this.database)
        const bearerController = new BearerController(bearerDataBase, this.cacheDB, undefined)
        const bearerHandler = new BearerHandler(bearerController, this.jwtManager)
        const bearerRoutes = new BearerRoutes(bearerHandler, this.authenticationMiddleware, this.cacheMiddleware)
        this.builder
            .setDatabase(bearerDataBase)
            .setController(bearerController)
            .setHandler(bearerHandler)
            .setRoutes(bearerRoutes)

        let encrypt = "false"
        if (process.env.CRYPTKEY && process.env.CRYPTIV) {
            const bearerCrypt = new CryptoManager(process.env.CRYPTKEY, process.env.CRYPTIV)
            this.builder.setEncryption(bearerCrypt)
            encrypt = "true"
        }

        console.log("- Bearer; DataBase: MongoDB; RedisCache: " + cache + "; Encryption: " + encrypt)
    }
    buildMongoWithCache(): void {
        throw new Error("Method not implemented.");
    }

    getRoutes(): BaseRoute[] {
        return this.builder.getRoutes() ? this.builder.getRoutes()!.routes : []
    }

    public setBuilder(builder: Builder<MongooseManager, BearerInterface>): void {
        this.builder = builder;
    }
}