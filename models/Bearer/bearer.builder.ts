import { ManagerInterface } from "../../framework/base/manager.interface";
import { RedisManager } from "../../framework/db/redis.manager";
import { Builder } from "../../framework/base/builder.interface";
import { BearerInterface } from "./bearer.model";
import { HandlerInterface, HandlerReadInterface } from "../../framework/base/handler.interface";
import { RoutesInterface, RoutesReadInterface } from "../../framework/base/route.interface";
import { CryptoManagerInterface } from "../../framework/utils/crypto.manager";
import { BearerController } from "../../controllers/bearer.controller";
import MongooseManager from "../../framework/db/mongoose.manager";

export class BearerDBBuilder implements Builder<MongooseManager, BearerInterface> {
    routes?: RoutesReadInterface<BearerInterface>;
    handler?: HandlerReadInterface<BearerInterface>;
    controller?: BearerController;
    database?: ManagerInterface<MongooseManager, BearerInterface>;
    cacheDataBase?: RedisManager
    cache: Boolean = false;
    encryptionManager?: CryptoManagerInterface; 

    setRoutes(routes: RoutesInterface<BearerInterface>): Builder<MongooseManager, BearerInterface> {
        this.routes = routes
        return this
    }
    setHandler(handler: HandlerInterface<BearerInterface> | HandlerReadInterface<BearerInterface>): Builder<MongooseManager, BearerInterface> {
        this.handler = handler
        if (this.routes) {
            this.routes.handler = handler
        }
        return this
    }
    setController(controller: BearerController): Builder<MongooseManager, BearerInterface> {
        this.controller = controller
        if (this.handler) {
            this.handler.controller = controller
        }
        return this
    }
    setDatabase(db: ManagerInterface<MongooseManager, BearerInterface>): Builder<MongooseManager, BearerInterface> {
        this.database = db
        if (this.controller) {
            this.controller.dataBase = db
        }
        return this
    }
    setCacheDB(redisManager: RedisManager): Builder<MongooseManager, BearerInterface> {
        this.cacheDataBase = redisManager
        return this
    }
    setCache(cache: Boolean): Builder<MongooseManager, BearerInterface> {
        this.cache = cache
        return this
    }
    setEncryption(encryptionManager: CryptoManagerInterface): Builder<MongooseManager, BearerInterface> {
        this.encryptionManager = encryptionManager
        if (this.controller) {
            this.controller.encryptionManager = encryptionManager
        }
        return this
    }
    getRoutes(): RoutesInterface<BearerInterface> | RoutesReadInterface<BearerInterface> | undefined {
        return this.routes
    }
    getHandler(): HandlerReadInterface<BearerInterface> | undefined {
        return this.handler
    }
    getController(): BearerController | undefined {
        return this.controller
    }
    getDatabase(): ManagerInterface<MongooseManager, BearerInterface> | undefined {
        return this.database
    }
    getCacheDB(): RedisManager | undefined {
        return this.cacheDataBase
    }
    getCache(): Boolean {
        return this.cache
    }
    getEncryption(): CryptoManagerInterface | undefined {
        return this.encryptionManager
    }
    build(): Builder<MongooseManager, BearerInterface> {
        return this
    }
   
}