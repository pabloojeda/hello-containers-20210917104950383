import { Mongoose } from "mongoose";

export default class BearerSchema {
    mongoose: Mongoose

    constructor(mongoose: Mongoose) {
        this.mongoose = mongoose;
        this.createModel();
    }
  
    createModel() {
        const BearerSchema = new this.mongoose.Schema(
            {
                token: {
                    type: String,
                    required: true
                },
                role: { 
                    type: String, 
                    enum: ['admin', 'member', 'none'],
                },
                userID: { 
                    type: String,
                    required: true
                },
                authID: { 
                    type: String,
                    required: true
                },
                apiKey: { 
                    type: String, 
                    required: true 
                },
                iat: {
                    type: Number
                },
                exp: {
                    type: Number
                }
            },
            {   
                toJSON: {
                    transform: function (doc, ret) {
                        ret.createdAt = ret.createdAt.toISOString()
                        ret.updatedAt = ret.updatedAt.toISOString()
                        ret._id = ret._id.toString()
                        delete ret.__v;
                    }
                },
                collection: 'bearer',
                timestamps: true 
            }
        )
        
        this.mongoose.model('Bearer', BearerSchema)
    }
}