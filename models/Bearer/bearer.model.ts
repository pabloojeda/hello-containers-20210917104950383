import { Identifiable, JSONInterface, ShortFieldsInterface } from "../../framework/base/basemodel.interface";
const cryptoFW = require('crypto');

export type Role =
  | 'admin'
  | 'member'
  | 'none'

export const getApiKey = function() : string {
    const initializationVector = cryptoFW.randomBytes(16)
    const initializationVectorString = initializationVector.toString('base64')
    return initializationVectorString
}

export interface BearerInterface extends Identifiable {
    token: string
    userID: string
    apiKey: string
    authID: string
    role?: string
    expiredAt?: string
}

export class Bearer implements BearerInterface, JSONInterface, ShortFieldsInterface<BearerInterface> {
    _id: string = "";
    token: string = ""
    userID: string = ""
    apiKey: string = "";
    authID: string = ""
    role: Role = 'none'
    expiredAt?: string = ""
    createdAt: Date = new Date();
    updatedAt: Date = new Date();

    constructor(token: string, userID: string, authID: string, apiKey: string, role: Role = "none", expiredAt?: string, createdAt: Date = new Date(), updatedAt: Date = new Date()) {
        this._id = userID
        this.token = token
        this.userID = userID
        this.authID = authID
        this.apiKey = apiKey;
        this.role = role
        this.expiredAt = expiredAt
        this.createdAt = createdAt
        this.updatedAt = updatedAt
    }

    toJSON(): { [key: string]: string } {
        return {
            _id: this._id,
            token: this.token,
            userID: this.userID,
            apiKey: this.apiKey,
            role: this.role ? this.role : "none",
            expiredAt: this.expiredAt ? this.expiredAt : "",
            createdAt: this.createdAt ? this.createdAt.toISOString() : "",
            updatedAt: this.updatedAt ? this.updatedAt.toISOString() : "",
        }
    }

    shortFields(): BearerInterface {
        return {
            _id: this._id ? this._id : "-",
            token: this.token,
            userID: this.userID,
            authID: this.authID,
            apiKey: this.apiKey,
            role: this.role ? this.role : "none",
            expiredAt: this.expiredAt ? this.expiredAt : "-"
        }
    }
}


