import { RedisManager } from "../../framework/db/redis.manager";
import { Builder } from "../../framework/base/builder.interface";
import { DirectorInterface, DirectorAuthInterface, DirectorInterfaceMongo } from "../../framework/base/director.interface";
import { AuthInterface } from "./auth.model";
import { BaseRoute } from "../../framework/base/route.interface";
import { AuthDBBuilder } from "./auth.builder";
import { AuthMiddlewareInterface } from "../../framework/utils/jwt.middleware.interface";
import { JWTManager } from "../../framework/utils/jwt.manager";
import { CacheMiddleware } from "../../framework/utils/cache.middleware";
import MongooseManager from "../../framework/db/mongoose.manager";
import { AuthManagerMongo } from "../../managers/Auth/auth.manager.mongo";
import { AuthController } from "../../controllers/auth.controller";
import { AuthHandler } from "../../handlers/auth.handler";
import { AuthRoutes } from "../../routes/auth.routes";
import { CryptoManager } from "../../framework/utils/crypto.manager";

export class AuthDBDirector implements DirectorInterface<MongooseManager, AuthInterface>, DirectorAuthInterface<MongooseManager, AuthInterface>, DirectorInterfaceMongo<MongooseManager, AuthInterface> {
    builder: Builder<MongooseManager, AuthInterface> = new AuthDBBuilder()
    cacheDB?: RedisManager
    database: MongooseManager
    authenticationMiddleware?: AuthMiddlewareInterface
    cacheMiddleware?: CacheMiddleware
    jwtManager?: JWTManager
    
    constructor(database: MongooseManager, cacheDB: RedisManager | undefined = undefined, authenticationMiddleware?: AuthMiddlewareInterface, cacheMiddleware?: CacheMiddleware, jwtManager?: JWTManager) {
        this.database = database
        this.cacheDB = cacheDB
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.jwtManager = jwtManager
    }
    buildMongo(): void {
        if (!this.jwtManager) {
            throw new Error("JWTManager is not provided");   
        }

        const authDataBase = new AuthManagerMongo(this.database)
        const authController = new AuthController(authDataBase, this.cacheDB, undefined)
        const authHandler = new AuthHandler(authController, this.jwtManager)
        const authRoutes = new AuthRoutes(authHandler, this.authenticationMiddleware, this.cacheMiddleware)
        this.builder
            .setDatabase(authDataBase)
            .setController(authController)
            .setHandler(authHandler)
            .setRoutes(authRoutes)

        let encrypt = "false"
        if (process.env.CRYPTKEY && process.env.CRYPTIV) {
            const authCrypt = new CryptoManager(process.env.CRYPTKEY, process.env.CRYPTIV)
            this.builder.setEncryption(authCrypt)
            encrypt = "true"
        }

        console.log("- Auth; DataBase: MongoDB;")
    }
    buildMongoWithCache(): void {
        throw new Error("Method not implemented.");
    }

    getRoutes(): BaseRoute[] {
        return this.builder.getRoutes() ? this.builder.getRoutes()!.routes : []
    }

    public setBuilder(builder: Builder<MongooseManager, AuthInterface>): void {
        this.builder = builder;
    }
}