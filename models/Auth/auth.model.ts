import { Identifiable, JSONInterface, ShortFieldsInterface } from "../../framework/base/basemodel.interface";
const cryptoFW = require('crypto');

export type Role =
  | 'admin'
  | 'member'
  | 'none'

export const getRole = function(role: string) : Role {
    switch (role) {
        case "admin":
            return "admin"
        case "member":
            return "member"
        default:
            return "none"
    }
}

export const getApiKey = function() : string {
    const initializationVector = cryptoFW.randomBytes(16)
    const initializationVectorString = initializationVector.toString('base64')
    return initializationVectorString
}

export interface AuthInterface extends Identifiable {
    userID: string
    apiKey: string
    role?: string
}

export class Auth implements AuthInterface, JSONInterface, ShortFieldsInterface<AuthInterface> {
    _id: string = "";
    userID: string = ""
    apiKey: string = "";
    role: Role = 'none'
    createdAt: Date = new Date();
    updatedAt: Date = new Date();

    constructor(userID: string, apiKey: string, role: Role = "none", createdAt: Date = new Date(), updatedAt: Date = new Date()) {
        this._id = userID
        this.userID = userID
        this.apiKey = apiKey;
        this.role = role
        this.createdAt = createdAt
        this.updatedAt = updatedAt
    }

    toJSON(): { [key: string]: string } {
        return {
            _id: this._id,
            userID: this.userID,
            apiKey: this.apiKey,
            role: this.role ? this.role : "none",
            createdAt: this.createdAt ? this.createdAt.toISOString() : "",
            updatedAt: this.updatedAt ? this.updatedAt.toISOString() : "",
        }
    }

    shortFields(): AuthInterface {
        return {
            _id: this._id ? this._id : "-",
            userID: this.userID,
            apiKey: this.apiKey,
            role: this.role ? this.role : "none",
        }
    }
}


