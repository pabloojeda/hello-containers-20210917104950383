import { ManagerInterface } from "../../framework/base/manager.interface";
import { RedisManager } from "../../framework/db/redis.manager";
import { Builder } from "../../framework/base/builder.interface";
import { AuthInterface } from "./auth.model";
import { HandlerInterface } from "../../framework/base/handler.interface";
import { RoutesInterface } from "../../framework/base/route.interface";
import { CryptoManagerInterface } from "../../framework/utils/crypto.manager";
import { AuthController } from "../../controllers/auth.controller";
import MongooseManager from "../../framework/db/mongoose.manager";

export class AuthDBBuilder implements Builder<MongooseManager, AuthInterface> {
    routes?: RoutesInterface<AuthInterface>;
    handler?: HandlerInterface<AuthInterface>;
    controller?: AuthController;
    database?: ManagerInterface<MongooseManager, AuthInterface>;
    cacheDataBase?: RedisManager
    cache: Boolean = false;
    encryptionManager?: CryptoManagerInterface; 

    setRoutes(routes: RoutesInterface<AuthInterface>): Builder<MongooseManager, AuthInterface> {
        this.routes = routes
        return this
    }
    setHandler(handler: HandlerInterface<AuthInterface>): Builder<MongooseManager, AuthInterface> {
        this.handler = handler
        if (this.routes) {
            this.routes.handler = handler
        }
        return this
    }
    setController(controller: AuthController): Builder<MongooseManager, AuthInterface> {
        this.controller = controller
        if (this.handler) {
            this.handler.controller = controller
        }
        return this
    }
    setDatabase(db: ManagerInterface<MongooseManager, AuthInterface>): Builder<MongooseManager, AuthInterface> {
        this.database = db
        if (this.controller) {
            this.controller.dataBase = db
        }
        return this
    }
    setCacheDB(redisManager: RedisManager): Builder<MongooseManager, AuthInterface> {
        this.cacheDataBase = redisManager
        return this
    }
    setCache(cache: Boolean): Builder<MongooseManager, AuthInterface> {
        this.cache = cache
        return this
    }
    setEncryption(encryptionManager: CryptoManagerInterface): Builder<MongooseManager, AuthInterface> {
        this.encryptionManager = encryptionManager
        if (this.controller) {
            this.controller.encryptionManager = encryptionManager
        }
        return this
    }
    getRoutes(): RoutesInterface<AuthInterface> | undefined {
        return this.routes
    }
    getHandler(): HandlerInterface<AuthInterface> | undefined {
        return this.handler
    }
    getController(): AuthController | undefined {
        return this.controller
    }
    getDatabase(): ManagerInterface<MongooseManager, AuthInterface> | undefined {
        return this.database
    }
    getCacheDB(): RedisManager | undefined {
        return this.cacheDataBase
    }
    getCache(): Boolean {
        return this.cache
    }
    getEncryption(): CryptoManagerInterface | undefined {
        return this.encryptionManager
    }
    build(): Builder<MongooseManager, AuthInterface> {
        return this
    }
   
}