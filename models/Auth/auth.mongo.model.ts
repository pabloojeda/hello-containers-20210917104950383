import { Mongoose } from "mongoose";

export default class AuthSchema {
    mongoose: Mongoose

    constructor(mongoose: Mongoose) {
        this.mongoose = mongoose;
        this.createModel();
    }
  
    createModel() {
        const AuthSchema = new this.mongoose.Schema(
            {
                role: { 
                    type: String, 
                    enum: ['admin', 'member', 'none'], 
                    required: true 
                },
                userID: { 
                    type: String,
                    required: true
                },
                apiKey: { 
                    type: String, 
                    required: true 
                }
            },
            {   
                toJSON: {
                    transform: function (doc, ret) {
                        ret.createdAt = ret.createdAt.toISOString()
                        ret.updatedAt = ret.updatedAt.toISOString()
                        ret._id = ret._id.toString()
                        delete ret.__v;
                    }
                },
                collection: 'auth',
                timestamps: true 
            }
        )
        
        this.mongoose.model('Auth', AuthSchema)
    }
}