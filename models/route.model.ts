import { RequestHandler as Middleware } from 'express';
import { Handler } from '../framework/base/handler.model'
import { BaseRoute, Method } from '../framework/base/route.interface';
export class Route implements BaseRoute {
    method: Method;
    path: string;
    middleware: Middleware[];
    handler: Handler;

    constructor(method: Method, path: string, middleware: Middleware[], handler: Handler) {
        this.method = method
        this.path = path
        this.middleware = middleware
        this.handler = handler
    }
}