import { LogInterface } from "../models/Logs/log.model";
import { LogSearchFields } from "../models/Logs/log.searchFields";
import { Pagination, PaginationInterface, PaginationResponseInterface } from "../framework/utils/pagination.interface";
import { ControllerInterface } from "../framework/base/controller.interface";
import { SearchFieldsInterface } from "../framework/utils/searchField.interface";
import { ManagerInterface } from "../framework/base/manager.interface";
import { RedisManager } from "../framework/db/redis.manager";
import MongooseManager from "../framework/db/mongoose.manager";
import { CryptoManagerInterface } from "../framework/utils/crypto.manager";

export class LogController implements ControllerInterface<LogInterface> {
    dataBase: ManagerInterface<MongooseManager, LogInterface>;
    encryptionManager: CryptoManagerInterface | undefined;
    cacheDataBase: RedisManager | undefined;

    constructor(dataBase: ManagerInterface<MongooseManager, LogInterface>, cacheDataBase: RedisManager | undefined = undefined, encryptionManager: CryptoManagerInterface | undefined = undefined) {
        this.dataBase = dataBase
        this.cacheDataBase = cacheDataBase
        this.encryptionManager = encryptionManager
    }
    
    async list(searchFields: SearchFieldsInterface | undefined, pagination: PaginationInterface | undefined): Promise<{items: LogInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const paginationResponse = pagination ? pagination : new Pagination()
            const searchFieldsRequest = searchFields ? searchFields : new LogSearchFields()
            const response = await this.dataBase.list(searchFieldsRequest, paginationResponse)
            return response
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async show(id: string): Promise<LogInterface | undefined> {
        try {
            if (!id) {
                return Promise.reject(new Error("Can not show a log without id"))
            }

            const log = await this.dataBase.show(id)
            if(!log) {
                return Promise.resolve(undefined)
            }
            return log
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async create(item: LogInterface): Promise<LogInterface> {
        try {
            if (!item || !item.log || !item.serviceProvider) {
                return Promise.reject(new Error('Something was wrong creating new Log... Use correct params.'))
            }
            const log = await this.dataBase.create(item)
            return log
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async update(id: string, item: LogInterface): Promise<LogInterface | undefined> {
        try {
            if (!id || !item || !item.log) {
                return Promise.reject(new Error('Something was wrong updating new Log... Use correct params.'))
            }
            let params : any = {}
            params.log = item.log
            if (item.serviceProvider) {
                params.serviceProvider = item.serviceProvider
            }
            const log = await this.dataBase.update(id, params)
            return log
        } catch (error) {
            return Promise.reject(error)
        }
    }
    async delete(id: string): Promise<LogInterface | undefined> {
        try {
            const log = await this.dataBase.delete(id)
            return log
        } catch (error) {
            return Promise.reject(error)
        }
    }

}


// https://docs.mongodb.com/v3.0/reference/operator/query/text/
// findById(), findOne(), findByIdAndRemove(), findByIdAndUpdate(), findOneAndRemove(), findOneAndUpdate()