import { PaginationResponseInterface } from "../framework/utils/pagination.interface";
import { ControllerInterface } from "../framework/base/controller.interface";
import { AuthInterface, getApiKey, getRole } from "../models/Auth/auth.model";
import { ManagerInterface } from "../framework/base/manager.interface";
import MongooseManager from "../framework/db/mongoose.manager";
import { CryptoManagerInterface } from "../framework/utils/crypto.manager";
import { RedisManager } from "../framework/db/redis.manager";

export class AuthController implements ControllerInterface<AuthInterface> {
    dataBase: ManagerInterface<MongooseManager, AuthInterface>;
    encryptionManager: CryptoManagerInterface | undefined;
    cacheDataBase: RedisManager | undefined;

    constructor(dataBase: ManagerInterface<MongooseManager, AuthInterface>, cacheDataBase: RedisManager | undefined = undefined, encryptionManager: CryptoManagerInterface | undefined = undefined) {
        this.dataBase = dataBase
        this.cacheDataBase = cacheDataBase
        this.encryptionManager = encryptionManager
    }
    
    async list(): Promise<{items: AuthInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const response = await this.dataBase.list(undefined, undefined)
            return response
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async show(user: string): Promise<AuthInterface | undefined> {
        try {
            const auth = await this.dataBase.show(user)
            if(!auth) {
                return Promise.resolve(undefined)
            }
            return auth
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async create(item: AuthInterface): Promise<AuthInterface> {
        try {
            if (!item.apiKey) {
                item.apiKey = getApiKey()
            }
            item.role = item.role? getRole(item.role) : getRole("")
            const auth = await this.dataBase.create(item)
            return auth
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async update(id: string, item: AuthInterface): Promise<AuthInterface | undefined> {
        try {
            const auth = await this.dataBase.update(id, item)
            return auth
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async delete(session: string): Promise<AuthInterface | undefined> {
        try {
            const auth = await this.dataBase.delete(session)
            return auth
        } catch (error) {
            return Promise.reject(error)
        }
    }
}

// https://docs.mongodb.com/v3.0/reference/operator/query/text/
// findById(), findOne(), findByIdAndRemove(), findByIdAndUpdate(), findOneAndRemove(), findOneAndUpdate()