import { PaginationResponseInterface } from "../framework/utils/pagination.interface";
import { ControllerInterface } from "../framework/base/controller.interface";
import { BearerInterface, getApiKey } from "../models/Bearer/bearer.model";
import { ManagerInterface } from "../framework/base/manager.interface";
import MongooseManager from "../framework/db/mongoose.manager";
import { CryptoManagerInterface } from "../framework/utils/crypto.manager";
import { RedisManager } from "../framework/db/redis.manager";

export class BearerController implements ControllerInterface<BearerInterface> {
    dataBase: ManagerInterface<MongooseManager, BearerInterface>;
    encryptionManager: CryptoManagerInterface | undefined;
    cacheDataBase: RedisManager | undefined;

    constructor(dataBase: ManagerInterface<MongooseManager, BearerInterface>, cacheDataBase: RedisManager | undefined = undefined, encryptionManager: CryptoManagerInterface | undefined = undefined) {
        this.dataBase = dataBase
        this.cacheDataBase = cacheDataBase
        this.encryptionManager = encryptionManager
    }
    
    async list(): Promise<{items: BearerInterface[], pagination: PaginationResponseInterface | undefined}> {
        try {
            const response = await this.dataBase.list(undefined, undefined)
            return response
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async show(user: string): Promise<BearerInterface | undefined> {
        try {
            const bearer = await this.dataBase.show(user)
            if(!bearer) {
                return Promise.resolve(undefined)
            }
            return bearer
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async create(item: BearerInterface): Promise<BearerInterface> {
        try {
            const bearer = await this.dataBase.create(item)
            return bearer
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async update(id: string, item: BearerInterface): Promise<BearerInterface | undefined> {
        try {
            const bearer = await this.dataBase.update(id, item)
            return bearer
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async delete(session: string): Promise<BearerInterface | undefined> {
        try {
            const bearer = await this.dataBase.delete(session)
            return bearer
        } catch (error) {
            return Promise.reject(error)
        }
    }
}

// https://docs.mongodb.com/v3.0/reference/operator/query/text/
// findById(), findOne(), findByIdAndRemove(), findByIdAndUpdate(), findOneAndRemove(), findOneAndUpdate()