import { JWTManagerInterface } from "../framework/utils/jwt.manager";
import { AuthMiddlewareInterface } from "../framework/utils/jwt.middleware.interface";
import { RequestHandler as Middleware } from 'express';
import { NextFunction, Request, Response } from 'express';

export class AuthMiddleware implements AuthMiddlewareInterface {
    jwt: JWTManagerInterface

    constructor(jwt: JWTManagerInterface) {
        this.jwt = jwt
    }

    authenticate: Middleware = (req, res, next) => {
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.split(' ')[1];
            this.jwt.verifyToken(token)
                .then(credentials => {
                    if (credentials) {
                        req.body.credentials = credentials
                        return next()
                    }
                    return res.sendStatus(401);
                })
                .catch(() => {
                    return res.sendStatus(403);
                })
        } else {
            res.sendStatus(401);
        }
    }

    hasRole(role: string) {
        return function(req: Request, res: Response, next: NextFunction) {
            if (req.body.credentials && req.body.credentials.role && req.body.credentials.role === role) {
                return next()
            } else {
               return res.sendStatus(401);
            }
        }
    }
}