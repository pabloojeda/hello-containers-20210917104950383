import { RequestHandler as Middleware } from 'express';

export const requestLogger: Middleware = (req, res, next) => {
  console.log(`${ req.method } ${ req.path } - \n query: ${ JSON.stringify(req.query) } \n body: ${ JSON.stringify(req.body) }`);
  next();
};