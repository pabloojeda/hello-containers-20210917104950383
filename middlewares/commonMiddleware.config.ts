const morgan = require('morgan')
var bodyParser = require("body-parser")
var path = require('path')
var rfs = require('rotating-file-stream')

export class ConfigCommonMiddleware {
    app: any

    constructor(app: any) {
        this.app = app;
    }

    // Apply common and necessary appwise middlewares.
    applyMiddlewares() {
        this.configCors()
        this.configBodyParser()
        this.configMorgan()
    }

    configBodyParser() {
        this.app.use(bodyParser.json());                                     
        this.app.use(bodyParser.urlencoded({extended: true}));               
        this.app.use(bodyParser.text());                                    
        this.app.use(bodyParser.json({ type: 'application/json'})); 
    }

    configMorgan() {
        morgan.token('body', (req: Request, res: Response) => JSON.stringify(req.body));
        morgan.token('headers', (req: Request, res: Response) => JSON.stringify(req.headers));
        morgan.token('query', (req: any, res: Response) => JSON.stringify(req.query));
        morgan.token("custom", `:remote-addr - :remote-user :date[clf] ":method :url HTTP/:http-version" :response-time ms - :status :res[content-length] ":user-agent"\n - headers: :headers\n - query: :query \n - body: :body`)
            
        if (process.env.ENV && process.env.ENV === "dev") {
            this.app.use(morgan('combined'))
        } 

        // create a rotating write stream
        var accessLogStream = rfs.createStream('access.log', {
            size:     '10M', // rotate every 10 MegaBytes written
            interval: '1d', // rotate daily
            compress: 'gzip', // compress rotated files
            path: path.join('logs')
        })
        // this.app.use(morgan('combined', { stream: accessLogStream }))
        // this.app.use(morgan('custom', { stream: accessLogStream }))
    }

    configCors() {
        const cors = require('cors');
        this.app.use(cors({ origin: true }));
        // const allowedOrigins = ["http://localhost:3000","http://localhost:8080","https://localhost:8101"];

        // this.app.use(
        //     cors({
        //         origin: function(origin: any, callback: any) {
        //             if (!origin) return callback(null, true);
        //             if (allowedOrigins.indexOf(origin) === -1) {
        //                 var msg =
        //                     "The CORS policy for this site does not " +
        //                     "allow access from the specified Origin.";
        //                 return callback(new Error(msg), false);
        //             }
        //             return callback(null, true);
        //         }
        //     })
        // ); 
    }
}